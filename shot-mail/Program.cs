﻿using shot_mail.Mailing;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Mail;

namespace shot_mail
{
	class Program
	{
		static void Main( string[] args )
		{
			var w = int.Parse( ConfigurationManager.AppSettings["screenW"] );
			var h = int.Parse( ConfigurationManager.AppSettings["screenH"] );
			var mailFrom = ConfigurationManager.AppSettings["mailFrom"];
			var mailTo = ConfigurationManager.AppSettings["mailTo"];

			using( var bitmap = new Bitmap( w, h ) )
			using ( Graphics g = Graphics.FromImage( bitmap ) )
			using ( var stream = new MemoryStream() )
			{
				g.CopyFromScreen( Point.Empty, Point.Empty, new Size( w, h ) );
				bitmap.Save( "test.jpeg", ImageFormat.Jpeg );
				bitmap.Save( stream, ImageFormat.Jpeg );

				var message = new MailMessage( mailFrom, mailTo, "Screenshot", "See attachment" );

				new EmailHelper().SendEmail( message, stream );
			}
		}
	}
}

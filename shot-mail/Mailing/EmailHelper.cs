﻿using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net.Mime;

namespace shot_mail.Mailing
{
	public class EmailHelper
	{
		public void SendEmail( MailMessage mailMessage, MemoryStream imageStream )
		{
			var smtpSection = (SmtpSection)ConfigurationManager.GetSection( "mailSettings/Gmail" );
			var credentials = new NetworkCredential( smtpSection.Network.UserName, smtpSection.Network.Password );

			imageStream.Position = 0;
			Attachment att = new Attachment( imageStream, new ContentType()
			{
				MediaType = MediaTypeNames.Image.Jpeg,
				Name = "screenshot.jpeg"
			} );

			mailMessage.Attachments.Add( att );

			using ( var client = new SmtpClient( smtpSection.Network.Host, smtpSection.Network.Port ) )
			{
				client.DeliveryMethod = smtpSection.DeliveryMethod;
				client.UseDefaultCredentials = smtpSection.Network.DefaultCredentials;
				client.Credentials = credentials;
				client.EnableSsl = smtpSection.Network.EnableSsl;

				client.Send( mailMessage );
			}
		}
	}
}
